import random
import time


def slow_function(i):
    print(f'Started {i}')
    start = time.time()
    time.sleep(random.randrange(1, 10))
    finish = time.time()
    print(f'Finished {i}: {finish - start}s')
