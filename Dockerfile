FROM python:3.7-alpine3.8

RUN pip install click
COPY test/test.py /test/test.py
COPY cli.py /cli.py
