import asyncio

import click

from test.test import slow_function


async def main(i):
    await asyncio.get_event_loop().run_in_executor(None, slow_function, i)


@click.command()
@click.argument('num')
def cli(num):
    loop = asyncio.get_event_loop()
    loop.run_until_complete(
        asyncio.gather(*[main(i) for i in range(int(num))])
    )


if __name__ == '__main__':
    cli()
